package hacktranslator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackTranslatorApplication {

  public static void main(String[] args) {
    SpringApplication.run(HackTranslatorApplication.class, args);
  }
}
