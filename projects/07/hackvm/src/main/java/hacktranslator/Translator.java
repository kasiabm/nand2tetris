package hacktranslator;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Translator {

  private final CommandHandler commandHandler;
  private int current;
  private int next;

  public Translator(final CommandHandler commandHandler) {
    this.commandHandler = commandHandler;
    current = 0;
    next = 1;
  }

  public List<String> translateCommands(final Path filePath, final List<String> commands) {
    final List<String> translation = new ArrayList<>();
    commands.forEach(command -> translation.add(translateCommand(filePath, command)));
    translation.add(Command.END.getCommand());
    translation.add(Command.AT_END.getCommand());
    translation.add(Command.JUMP.getCommand() + "\n");
    return translation;
  }

  private String translateCommand(final Path filePath, final String command) {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner.add("// " + command);
    if (command.matches("push\\s+constant\\s+\\d+")) {
      joiner.add(commandHandler.pushConstant(command));
    } else if (command.matches("add")) {
      joiner.add(commandHandler.add());
    } else if (command.matches("sub")) {
      joiner.add(commandHandler.sub());
    } else if (command.matches("and")) {
      joiner.add(commandHandler.and());
    } else if (command.matches("or")) {
      joiner.add(commandHandler.or());
    } else if (command.matches("not")) {
      joiner.add(commandHandler.not());
    } else if (command.matches("neg")) {
      joiner.add(commandHandler.neg());
    } else if (command.matches("eq")) {
      joiner.add(commandHandler.eq(current, next));
      current++;
      next++;
    } else if (command.matches("gt")) {
      joiner.add(commandHandler.gt(current, next));
      current++;
      next++;
    } else if (command.matches("lt")) {
      joiner.add(commandHandler.lt(current, next));
      current++;
      next++;
    } else if (command.matches("pop\\s+local\\s+\\d+")) {
      joiner.add(commandHandler.popToMemory(command));
    } else if (command.matches("push\\s+local\\s+\\d+")) {
      joiner.add(commandHandler.pushFromMemoryOnStack(command));
    } else if (command.matches("pop\\s+argument\\s+\\d+")) {
      joiner.add(commandHandler.popToMemory(command));
    } else if (command.matches("push\\s+argument\\s+\\d+")) {
      joiner.add(commandHandler.pushFromMemoryOnStack(command));
    } else if (command.matches("pop\\s+this\\s+\\d+")) {
      joiner.add(commandHandler.popToMemory(command));
    } else if (command.matches("push\\s+this\\s+\\d+")) {
      joiner.add(commandHandler.pushFromMemoryOnStack(command));
    } else if (command.matches("pop\\s+that\\s+\\d+")) {
      joiner.add(commandHandler.popToMemory(command));
    } else if (command.matches("push\\s+that\\s+\\d+")) {
      joiner.add(commandHandler.pushFromMemoryOnStack(command));
    } else if (command.matches("pop\\s+temp\\s+\\d+")) {
      joiner.add(commandHandler.popToMemory(command));
    } else if (command.matches("push\\s+temp\\s+\\d+")) {
      joiner.add(commandHandler.pushFromMemoryOnStack(command));
    } else if (command.matches("pop\\s+pointer\\s+\\d+")) {
      joiner.add(commandHandler.popPointer(command));
    } else if (command.matches("push\\s+pointer\\s+\\d+")) {
      joiner.add(commandHandler.pushPointer(command));
    } else if (command.matches("pop\\s+static\\s+\\d+")) {
      joiner.add(commandHandler.popStatic(filePath, command));
    } else if (command.matches("push\\s+static\\s+\\d+")) {
      joiner.add(commandHandler.pushStatic(filePath, command));
    }
    return joiner.toString();
  }
}
