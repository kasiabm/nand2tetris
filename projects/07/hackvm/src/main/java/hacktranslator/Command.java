package hacktranslator;

public enum Command {
  CONSTANT("@%s"),
  GET_STACK_POINTER("@SP"),
  GET_LOCAL("@LCL"),
  GET_ARG("@ARG"),
  GET_THIS("@THIS"),
  GET_THAT("@THAT"),
  GET_TEMP("@TEMP"),
  SET_ADDRESS("A=M"),
  PUSH("M=D"),
  POP("D=M"),
  SET_REGISTER_FROM_ADDRESS("D=A"),
  INCREASE_POINTER("M=M+1"),
  DECREASE_POINTER("M=M-1"),
  AND("D=D&M"),
  OR("D=D|M"),
  NOT("D=!M"),
  NEG("D=-M"),
  ADD_MEM_TO_REG("D=D+M"),
  ADD_REG_TO_MEM("M=M+D"),
  SUB_MEM_FROM_REG_INTO_REG("D=D-M"),
  SUB_REG_FROM_MEM_INTO_REG("D=M-D"),
  SUB_REG_FROM_MEM_INTO_MEM("M=M-D"),
  SET_TRUE("M=-1"),
  SET_FALSE("M=0"),
  EQ("(EQ_%d)"),
  GT("(GT_%d)"),
  LT("(LT_%d)"),
  ELSE("(ELSE_%d)"),
  NEXT("(NEXT_%d)"),
  END("(END)"),
  AT_EQ("@EQ_%d"),
  AT_NEXT("@NEXT_%d"),
  AT_ELSE("@ELSE_%d"),
  AT_LESS_THAN("@LT_%d"),
  AT_GREATER_THAN("@GT_%d"),
  AT_END("@END"),
  JUMP("0;JMP"),
  JUMP_EQ("D;JEQ"),
  JUMP_NOT_EQ("D;JNE"),
  JUMP_LESS_THAN("D;JLT"),
  JUMP_LESS_OR_EQ("D;JLE"),
  JUMP_GREATER_THAN("D;JGT"),
  JUMP_GREATER_OR_EQ("D;JGE");

  private final String command;

  Command(final String command) {
    this.command = command;
  }

  public String getCommand() {
    return command;
  }
}
