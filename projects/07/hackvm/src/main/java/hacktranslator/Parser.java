package hacktranslator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

  private final Path vmFilePath;

  public Parser(final Path vmFilePath) {
    this.vmFilePath = vmFilePath;
  }

  public Path getVmFilePath() {
    return vmFilePath;
  }

  public List<String> parse() throws IOException {
    return readCommands();
  }

  private List<String> readCommands() throws IOException {
    final Stream<String> all = Files.lines(Paths.get(vmFilePath.toString()));
    return all
        .filter(line -> !line.isEmpty() && !line.startsWith("//"))
        .map(line -> removeInlineComments(line).trim())
        .collect(Collectors.toList());
  }

  private String removeInlineComments(final String line) {
    if (line.contains("//")) {
      return line.substring(0, line.indexOf("//"));
    }
    return line;
  }
}
