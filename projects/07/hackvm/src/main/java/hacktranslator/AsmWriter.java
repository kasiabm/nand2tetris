package hacktranslator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.StringJoiner;

public class AsmWriter {

  private final Path asmFilePath;

  public AsmWriter(final Path vmFilePath) {
    asmFilePath = setAsmFilePath(vmFilePath);
  }

  public Path getAsmFilePath() {
    return asmFilePath;
  }

  private Path setAsmFilePath(final Path vmFilePath) {
    final String vmFileName = vmFilePath.toString();
    final String asmFileName = vmFileName
        .substring(0, vmFileName.indexOf(".")) + ".asm";
    return Paths.get(asmFileName);
  }

  public void writeCommands(final List<String> commands) throws IOException {
    final StringJoiner stringJoiner = new StringJoiner("\n");
    commands.forEach(stringJoiner::add);
    Files.write(Paths.get(asmFilePath.toString()),
        stringJoiner.toString().getBytes());
  }
}
