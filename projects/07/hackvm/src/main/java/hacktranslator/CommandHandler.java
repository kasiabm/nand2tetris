package hacktranslator;

import java.nio.file.Path;
import java.util.StringJoiner;

public class CommandHandler {

  public String pushConstant(final String command) {
    final StringJoiner joiner = new StringJoiner("\n");
    final String constant = getConstantValue(command);
    joiner
        .add(String.format(Command.CONSTANT.getCommand(), constant))
        .add(Command.SET_REGISTER_FROM_ADDRESS.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand());
    return joiner.toString();
  }

  public String add() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.ADD_MEM_TO_REG.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String sub() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.SUB_REG_FROM_MEM_INTO_REG.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String and() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.AND.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String or() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.OR.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String not() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.NOT.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String neg() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.NEG.getCommand())
        .add(pushOnStack());
    return joiner.toString();
  }

  public String eq(final int current, final int next) {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.SUB_MEM_FROM_REG_INTO_REG.getCommand())
        .add(String.format(Command.AT_EQ.getCommand(), current))
        .add(Command.JUMP_EQ.getCommand())
        .add(String.format(Command.AT_ELSE.getCommand(), current))
        .add(Command.JUMP_NOT_EQ.getCommand())
        .add(String.format(Command.EQ.getCommand(), current))
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.SET_TRUE.getCommand())
        .add(addConditionalJumps(current, next));
    return joiner.toString();
  }

  public String gt(final int current, final int next) {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.SUB_REG_FROM_MEM_INTO_REG.getCommand())
        .add(String.format(Command.AT_GREATER_THAN.getCommand(), current))
        .add(Command.JUMP_GREATER_THAN.getCommand())
        .add(String.format(Command.AT_ELSE.getCommand(), current))
        .add(Command.JUMP_LESS_OR_EQ.getCommand())
        .add(String.format(Command.GT.getCommand(), current))
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.SET_TRUE.getCommand())
        .add(addConditionalJumps(current, next));
    return joiner.toString();
  }

  public String lt(final int current, final int next) {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(getLastTwoPointers())
        .add(Command.SUB_REG_FROM_MEM_INTO_REG.getCommand())
        .add(String.format(Command.AT_LESS_THAN.getCommand(), current))
        .add(Command.JUMP_LESS_THAN.getCommand())
        .add(String.format(Command.AT_ELSE.getCommand(), current))
        .add(Command.JUMP_GREATER_OR_EQ.getCommand())
        .add(String.format(Command.LT.getCommand(), current))
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.SET_TRUE.getCommand())
        .add(addConditionalJumps(current, next));
    return joiner.toString();
  }

  public String popToMemory(final String command) {
    final StringJoiner joiner = new StringJoiner("\n");
    final String pointer = getPointer(command);
    final String constant = getConstantValue(command);
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(String.format(Command.CONSTANT.getCommand(), constant))
        .add(Command.SET_REGISTER_FROM_ADDRESS.getCommand())
        .add(pointer)
        .add(Command.ADD_REG_TO_MEM.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.POP.getCommand())
        .add(pointer)
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(String.format(Command.CONSTANT.getCommand(), constant))
        .add(Command.SET_REGISTER_FROM_ADDRESS.getCommand())
        .add(pointer)
        .add(Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand());
    return joiner.toString();
  }

  public String pushFromMemoryOnStack(final String command) {
    final StringJoiner joiner = new StringJoiner("\n");
    final String pointer = getPointer(command);
    final String constant = getConstantValue(command);
    joiner
        .add(String.format(Command.CONSTANT.getCommand(), constant))
        .add(Command.SET_REGISTER_FROM_ADDRESS.getCommand())
        .add(pointer)
        .add(Command.ADD_REG_TO_MEM.getCommand())
        .add(pointer)
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.POP.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(String.format(Command.CONSTANT.getCommand(), constant))
        .add(Command.SET_REGISTER_FROM_ADDRESS.getCommand())
        .add(pointer)
        .add(Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand());
    return joiner.toString();
  }

  public String popPointer(final String command) {
    final String pointerValue = getConstantValue(command);
    final StringJoiner joiner = new StringJoiner("\n");
    joiner.add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.POP.getCommand())
        .add(getThisThatPointer(pointerValue))
        .add(Command.PUSH.getCommand());
    return joiner.toString();
  }

  public String pushPointer(final String command) {
    final StringJoiner joiner = new StringJoiner("\n");
    final String pointerValue = getConstantValue(command);
    joiner
        .add(getThisThatPointer(pointerValue))
        .add(Command.POP.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand());
    return joiner.toString();
  }

  private String getThisThatPointer(final String pointerValue) {
    return "0".equals(pointerValue) ? Command.GET_THIS.getCommand() : Command.GET_THAT.getCommand();
  }

  public CharSequence popStatic(final Path filePath, String command) {
    final String vmFileName = filePath.getFileName().toString();
    final String constant = vmFileName.substring(0, vmFileName.indexOf("."));
    final String pointerValue = getConstantValue(command);
    final StringJoiner joiner = new StringJoiner("\n");
    joiner.add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.POP.getCommand())
        .add(String.format("@%s.%s", constant, pointerValue))
        .add(Command.PUSH.getCommand());
    return joiner.toString();
  }

  public CharSequence pushStatic(final Path filePath, String command) {
    final String vmFileName = filePath.getFileName().toString();
    final String constant = vmFileName.substring(0, vmFileName.indexOf("."));
    final String pointerValue = getConstantValue(command);
    final StringJoiner joiner = new StringJoiner("\n");
    joiner.add(String.format("@%s.%s", constant, pointerValue))
        .add(Command.POP.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand());
    return joiner.toString();
  }

  private String getConstantValue(final String command) {
    return command.split("\\s+")[2];
  }

  private String getLastTwoPointers() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.POP.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.DECREASE_POINTER.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand());
    return joiner.toString();
  }

  private String pushOnStack() {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.PUSH.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand());
    return joiner.toString();
  }

  private String addConditionalJumps(final int current, final int next) {
    final StringJoiner joiner = new StringJoiner("\n");
    joiner
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand())
        .add(String.format(Command.AT_NEXT.getCommand(), next))
        .add(Command.JUMP.getCommand())
        .add(String.format(Command.ELSE.getCommand(), current))
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.SET_ADDRESS.getCommand())
        .add(Command.SET_FALSE.getCommand())
        .add(Command.GET_STACK_POINTER.getCommand())
        .add(Command.INCREASE_POINTER.getCommand())
        .add(String.format(Command.AT_NEXT.getCommand(), next))
        .add(Command.JUMP.getCommand())
        .add(String.format(Command.NEXT.getCommand(), next));
    return joiner.toString();
  }

  private String getPointer(final String command) {
    final String memoryBlock = command.split("\\s+")[1];
    switch (memoryBlock) {
      case "local":
        return Command.GET_LOCAL.getCommand();
      case "argument":
        return Command.GET_ARG.getCommand();
      case "this":
      case "pointer":
        return Command.GET_THIS.getCommand();
      case "that":
        return Command.GET_THAT.getCommand();
      case "temp":
        return Command.GET_TEMP.getCommand();
    }
    return "";
  }
}
