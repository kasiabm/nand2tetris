package hacktranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AsmWriterTest {

  private static final Path RESOURCE_DIRECTORY =
      Paths.get("src", "test", "resources");
  private Parser parser;
  private AsmWriter asmWriter;
  private Translator translator;

  @BeforeEach
  public void beforeEachTest() {
    translator = new Translator(new CommandHandler());
  }

  @Test
  public void shouldInitialiseAsmWriter() {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/simpleadd/SimpleAdd.asm");

    parser = new Parser(path);
    asmWriter = new AsmWriter(path);

    assertNotNull(asmWriter.getAsmFilePath());
    assertEquals(expected, asmWriter.getAsmFilePath());
  }

  @Test
  public void shouldWriteSimpleAddToAsmFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/simpleadd/SimpleAddExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser = new Parser(path);
    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());
    asmWriter = new AsmWriter(path);
    asmWriter.writeCommands(translation);

    final Path actual = Paths.get(RESOURCE_DIRECTORY
        + "/simpleadd/SimpleAdd.asm");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i),
            actualContent.get(i)));
  }

  @Test
  public void shouldWriteStackTestToAsmFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/stacktest/StackTest.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/stacktest/StackTestExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser = new Parser(path);
    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());

    asmWriter = new AsmWriter(path);
    asmWriter.writeCommands(translation);

    final Path actual = Paths.get(RESOURCE_DIRECTORY
        + "/stacktest/StackTest.asm");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i), actualContent.get(i)));
  }

  @Test
  public void shouldWriteBasicTestToAsmFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/basictest/BasicTest.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/basictest/BasicTestExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser = new Parser(path);
    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());

    asmWriter = new AsmWriter(path);
    asmWriter.writeCommands(translation);

    final Path actual = Paths.get(RESOURCE_DIRECTORY
        + "/basictest/BasicTest.asm");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i), actualContent.get(i)));
  }

  @Test
  public void shouldWritePointerTestToAsmFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/pointertest/PointerTest.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/pointertest/PointerTestExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser = new Parser(path);
    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());

    asmWriter = new AsmWriter(path);
    asmWriter.writeCommands(translation);

    final Path actual = Paths.get(RESOURCE_DIRECTORY
        + "/pointertest/PointerTest.asm");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i), actualContent.get(i)));
  }

  @Test
  public void shouldWriteStaticTestToAsmFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/statictest/StaticTest.vm");
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/statictest/StaticTestExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser = new Parser(path);
    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());

    asmWriter = new AsmWriter(path);
    asmWriter.writeCommands(translation);

    final Path actual = Paths.get(RESOURCE_DIRECTORY
        + "/statictest/StaticTest.asm");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i), actualContent.get(i)));
  }
}
