package hacktranslator;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootTest
public class HackTranslatorApplicationTest {

  private static final Path RESOURCE_DIRECTORY =
      Paths.get("src", "test", "resources");

  @Test
  public void contextLoads() {}

  @Test
  public void shouldReturnTranslatedVMFile() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");
    final Parser parser = new Parser(path);
    final Translator translator = new Translator(new CommandHandler());
    final AsmWriter asmWriter = new AsmWriter(path);

    asmWriter.writeCommands(translator.translateCommands(path, parser.parse()));
  }
}
