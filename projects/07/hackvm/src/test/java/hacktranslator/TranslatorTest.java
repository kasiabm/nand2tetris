package hacktranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TranslatorTest {

  private static final Path RESOURCE_DIRECTORY =
      Paths.get("src", "test", "resources");
  private static final String AT_NEXT = "@NEXT_1\n0;JMP\n";
  private static final String NEW_LINE = "\n";
  private Parser parser;
  private Translator translator;
  private List<String> commands;

  @BeforeEach
  public void beforeEachTest() {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");
    parser = new Parser(path);
    final CommandHandler commandHandler = new CommandHandler();
    translator = new Translator(commandHandler);
    commands = new ArrayList<>();
  }

  @Test
  public void shouldTranslatePushConstant() {
    commands.add("push constant 7");
    assertEquals("// push constant 7\n"
            + "@7\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateAnd() {
    commands.add("and");
    assertEquals("// and\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.AND.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateOr() {
    commands.add("or");
    assertEquals("// or\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.OR.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateNot() {
    commands.add("not");
    assertEquals("// not\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.NOT.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateAdd() {
    commands.add("add");
    assertEquals("// add\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.ADD_MEM_TO_REG.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateSubFromRegister() {
    commands.add("sub");
    assertEquals("// sub\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_REG.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateNeg() {
    commands.add("neg");
    assertEquals("// neg\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.NEG.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateEq() {
    commands.add("eq");
    assertEquals("// eq\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SUB_MEM_FROM_REG_INTO_REG.getCommand() + NEW_LINE
            + "@EQ_0\n"
            + "D;JEQ\n"
            + "@ELSE_0\n"
            + "D;JNE\n"
            + "(EQ_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_TRUE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand() + NEW_LINE
            + AT_NEXT
            + "(ELSE_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_FALSE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand() + NEW_LINE
            + AT_NEXT
            + "(NEXT_1)",
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateGt() {
    commands.add("gt");
    assertEquals("// gt\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_REG.getCommand() + NEW_LINE
            + "@GT_0\n"
            + "D;JGT\n"
            + "@ELSE_0\n"
            + "D;JLE\n"
            + "(GT_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_TRUE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand() + NEW_LINE
            + AT_NEXT
            + "(ELSE_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_FALSE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand() + NEW_LINE
            + AT_NEXT
            + "(NEXT_1)",
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateLt() {
    commands.add("lt");
    assertEquals("// lt\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_REG.getCommand() + NEW_LINE
            + "@LT_0\n"
            + "D;JLT\n"
            + "@ELSE_0\n"
            + "D;JGE\n"
            + "(LT_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_TRUE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand()
            + NEW_LINE
            + AT_NEXT
            + "(ELSE_0)\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.SET_FALSE.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand()
            + NEW_LINE
            + AT_NEXT
            + "(NEXT_1)",
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopLocal() {
    commands.add("pop local 0");
    assertEquals("// pop local 0\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + "@0\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@0\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopArgument() {
    commands.add("pop argument 2");
    assertEquals("// pop argument 2\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + "@2\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@2\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopThis() {
    commands.add("pop this 6");
    assertEquals("// pop this 6\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopThat() {
    commands.add("pop that 5");
    assertEquals("// pop that 5\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + "@5\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@5\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopTemp() {
    commands.add("pop temp 6");
    assertEquals("// pop temp 6\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushLocal() {
    commands.add("push local 0");
    assertEquals("// push local 0\n"
            + "@0\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@0\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_LOCAL.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushArgument() {
    commands.add("push argument 1");
    assertEquals("// push argument 1\n"
            + "@1\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@1\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_ARG.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushThis() {
    commands.add("push this 6");
    assertEquals("// push this 6\n"
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushThat() {
    commands.add("push that 5");
    assertEquals("// push that 5\n"
            + "@5\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@5\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushTemp() {
    commands.add("push temp 6");
    assertEquals("// push temp 6\n"
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.ADD_REG_TO_MEM.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + "@6\n"
            + Command.SET_REGISTER_FROM_ADDRESS.getCommand() + NEW_LINE
            + Command.GET_TEMP.getCommand() + NEW_LINE
            + Command.SUB_REG_FROM_MEM_INTO_MEM.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopThisPointer() {
    commands.add("pop pointer 0");
    assertEquals("// pop pointer 0\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopThatPointer() {
    commands.add("pop pointer 1");
    assertEquals("// pop pointer 1\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.PUSH.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushThisPointer() {
    commands.add("push pointer 0");
    assertEquals("// push pointer 0\n"
            + Command.GET_THIS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushThatPointer() {
    commands.add("push pointer 1");
    assertEquals("// push pointer 1\n"
            + Command.GET_THAT.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePopStatic() {
    commands.add("pop static 8");
    assertEquals("// pop static 8\n"
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.DECREASE_POINTER.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + "@SimpleAdd.8" + NEW_LINE
            + Command.PUSH.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslatePushStatic() {
    commands.add("push static 3");
    assertEquals("// push static 3\n"
            + "@SimpleAdd.3" + NEW_LINE
            + Command.POP.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.SET_ADDRESS.getCommand() + NEW_LINE
            + Command.PUSH.getCommand() + NEW_LINE
            + Command.GET_STACK_POINTER.getCommand() + NEW_LINE
            + Command.INCREASE_POINTER.getCommand(),
        translator.translateCommands(parser.getVmFilePath(), commands).get(0));
  }

  @Test
  public void shouldTranslateAllCommands() throws IOException {
    final Path expected = Paths.get(RESOURCE_DIRECTORY
        + "/simpleadd/SimpleAddExpected.asm");
    final List<String> expectedContent = Files.readAllLines(expected);

    final List<String> translation = translator.translateCommands(parser.getVmFilePath(),
        parser.parse());
    final List<String> actualContent = new ArrayList<>();
    translation.forEach(line ->
        actualContent.addAll(Arrays.asList(line.split("\n")))
    );

    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i), actualContent.get(i)));
  }
}
