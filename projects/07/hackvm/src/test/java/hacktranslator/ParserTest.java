package hacktranslator;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class ParserTest {

  private static final Path RESOURCE_DIRECTORY =
      Paths.get("src", "test", "resources");

  @Test
  public void shouldInitialiseParser() {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");

    final Parser parser = new Parser(path);

    assertNotNull(parser.getVmFilePath());
    assertEquals(path, parser.getVmFilePath());
  }

  @Test
  public void shouldReadAllCommands() throws IOException {
    final Path path = Paths.get(RESOURCE_DIRECTORY + "/simpleadd/SimpleAdd.vm");
    final Parser parser = new Parser(path);

    assertEquals(3, parser.parse().size());
  }
}
