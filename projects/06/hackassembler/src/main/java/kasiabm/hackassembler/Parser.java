package kasiabm.hackassembler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

  private final Map<Integer, String> commands;
  private BinaryCommand binaryCommand;
  private final SymbolTable symbolTable;
  private final Code code;

  public Parser() {
    commands = new HashMap<>();
    binaryCommand = new BinaryCommand();
    symbolTable = new SymbolTable();
    code = new Code();
  }

  public Path parse(final String asmFileName) throws IOException {
    final int[] counter = {0};
    final List<String> commandsToProcess = getCommands(asmFileName);
    processCommands(commandsToProcess, counter);
    updateSymbols(commandsToProcess);
    return writeToHackFile(asmFileName);
  }

  private List<String> getCommands(final String asmFileName) throws IOException {
    final Stream<String> all = Files.lines(Paths.get(asmFileName));
    return all
        .filter(line -> !line.isEmpty() && !line.startsWith("//"))
        .map(line -> removeInlineComments(line).trim())
        .collect(Collectors.toList());
  }

  private String removeInlineComments(final String line) {
    if (line.contains("//")) {
      return line.substring(0, line.indexOf("//"));
    }
    return line;
  }

  private void processCommands(final List<String> commandsToProcess,
                               final int[] counter) {
    commandsToProcess.forEach(line -> {
      if (!line.startsWith("(")) {
        final String processedCommand = binaryCommand.toBinary(line,
            counter[0], symbolTable, code);
        commands.put(counter[0], processedCommand);
        counter[0]++;
      } else {
        symbolTable.addSymbol(line, counter[0]);
      }
    });
  }

  private void updateSymbols(List<String> lines) {
    final int[] counter = {0};
    lines.forEach(command -> {
      final String minusAt = binaryCommand.commandMinusAt(command);
      if (command.matches("@[A-Z0-9_.$:]+") && symbolTable.contains(minusAt)) {
        final int address = symbolTable.getAddress(minusAt);
        final String binary = binaryCommand.commandInBinary(address);
        commands.put(counter[0], binary);
      }
      counter[0]++;
    });
  }

  private Path writeToHackFile(final String asmFileName) throws IOException {
    final String hackFileName = asmFileName.substring(0, asmFileName.indexOf(
        ".")) + ".hack";
    final StringJoiner stringJoiner = new StringJoiner("\n");
    commands.forEach((k, v) -> stringJoiner.add(v));
    return Files.write(Paths.get(hackFileName),
        stringJoiner.toString().getBytes());
  }
}
