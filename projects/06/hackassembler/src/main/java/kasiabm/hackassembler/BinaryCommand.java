package kasiabm.hackassembler;

public class BinaryCommand {

  public String toBinary(final String command, final int counter,
                         final SymbolTable symbolTable, final Code code) {
    if (command.matches("@\\d+")) {
      return getAddressFromACommand(command);
    } else if (command.matches("@[A-Z0-9_.$:]+")
        && symbolTable.contains(command.substring(command.indexOf("@")))) {
      final int address =
          symbolTable.getAddress(command.substring(command.indexOf("@")));
      return commandInBinary(address);
    } else if (command.matches("@\\w+") && !symbolTable.contains(command)) {
      symbolTable.addSymbol(command, counter);
      return commandInBinary(counter);
    } else {
      return cCommandInBinary(command, code);
    }
  }

  private String getAddressFromACommand(final String command) {
    final String value = commandMinusAt(command);
    return commandInBinary(value);
  }

  private String commandInBinary(final String command) {
    final String binary = Integer.toBinaryString(Integer.parseInt(command));
    return String.format("%16s", binary).replace(' ', '0');
  }

  public String commandInBinary(final int command) {
    final String binary = Integer.toBinaryString(command);
    return String.format("%16s", binary).replace(' ', '0');
  }

  private String cCommandInBinary(final String command, final Code code) {
    return String.format("111%s%s%S", code.comp(command), code.dest(command),
        code.jump(command));
  }

  public String commandMinusAt(final String command) {
    return command.substring(command.indexOf("@") + 1);
  }
}
