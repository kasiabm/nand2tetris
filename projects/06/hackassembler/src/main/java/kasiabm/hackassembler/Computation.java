package kasiabm.hackassembler;

public enum Computation {

  ZERO("0", "0101010"),
  ONE("1", "0111111"),
  MINUS_ONE("-1", "0111010"),
  D("D", "0001100"),
  A("A", "0110000"),
  M("M", "1110000"),
  N0T_D("!D", "0001101"),
  NOT_A("!A", "0110001"),
  NOT_M("!M", "1110001"),
  MINUS_D("-D", "0001111"),
  MINUS_A("-A", "0110011"),
  MINUS_M("-M", "1110011"),
  D_PLUS_ONE("D+1", "0011111"),
  A_PLUS_ONE("A+1", "0110111"),
  M_PLUS_ONE("M+1", "1110111"),
  D_MINUS_ONE("D-1", "0001110"),
  A_MINUS_ONE("A-1", "0110010"),
  M_MINUS_ONE("M-1", "1110010"),
  D_PLUS_A("D+A", "0000010"),
  D_PLUS_M("D+M", "1000010"),
  D_MINUS_A("D-A", "0010011"),
  D_MINUS_M("D-M", "1010011"),
  A_MINUS_D("A-D", "0000111"),
  M_MINUS_D("M-D", "1000111"),
  D_AND_A("D&A", "0000000"),
  D_AND_M("D&M", "1000000"),
  D_OR_A("D|A", "0010101"),
  D_OR_M("D|M", "1010101");

  private final String command;
  private final String computation;

  Computation(final String command, final String computation) {
    this.command = command;
    this.computation = computation;
  }

  public static String getComputation(final String command) {
    for (final Computation computation : Computation.values()) {
      if (computation.getCommand().equals(command)) {
        return computation.getComputation();
      }
    }
    throw new IllegalArgumentException("Unrecognised command: " + command);
  }

  private String getCommand() {
    return command;
  }

  private String getComputation() {
    return computation;
  }
}
