package kasiabm.hackassembler;

public enum Destination {

  NULL("000"),
  M("001"),
  D("010"),
  MD("011"),
  A("100"),
  AM("101"),
  AD("110"),
  AMD("111");

  private final String destination;

  Destination(final String destination) {
    this.destination = destination;
  }

  public static String getDestination(final String dest) {
    if (dest == null) {
      return NULL.getDestination();
    }
    for (final Destination destination : Destination.values()) {
      if (destination.toString().equals(dest)) {
        return destination.getDestination();
      }
    }
    throw new IllegalArgumentException("Unrecognised destination: " + dest);
  }

  private String getDestination() {
    return destination;
  }
}
