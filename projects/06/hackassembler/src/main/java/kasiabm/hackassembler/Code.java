package kasiabm.hackassembler;

public class Code {

  private static final String EQUALS = "=";
  private static final String SEMICOLON = ";";
  private static final int OFFSET = 1;

  public String dest(final String command) {
    final String destination = destPresent(command)
        ? command.substring(0, command.indexOf(EQUALS)) : null;
    return Destination.getDestination(destination);
  }

  private boolean destPresent(final String command) {
    return command != null && command.contains(EQUALS);
  }

  public String jump(final String command) {
    final String jump = jumpPresent(command)
        ? command.substring(command.indexOf(SEMICOLON) + OFFSET) : null;
    return Jump.getJump(jump);
  }

  private boolean jumpPresent(final String command) {
    return command != null && command.contains(SEMICOLON);
  }

  public String comp(final String command) {
    final String comp = destPresent(command)
        ? command.substring(command.indexOf(EQUALS) + OFFSET)
        : command.substring(0, command.indexOf(SEMICOLON));
    return Computation.getComputation(comp);
  }
}
