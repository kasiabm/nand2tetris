package kasiabm.hackassembler;

public enum Jump {

  NULL("000"),
  JGT("001"),
  JEQ("010"),
  JGE("011"),
  JLT("100"),
  JNE("101"),
  JLE("110"),
  JMP("111");

  private final String jump;

  Jump(final String jump) {
    this.jump = jump;
  }

  public static String getJump(final String jmp) {
    if (jmp == null) {
      return NULL.getJump();
    }
    for (final Jump jump : Jump.values()) {
      if (jump.toString().equals(jmp)) {
        return jump.getJump();
      }
    }
    throw new IllegalArgumentException("Unrecognised jump: " + jmp);
  }

  private String getJump() {
    return jump;
  }
}
