package kasiabm.hackassembler;

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {

  private static final int OFFSET = 1;
  private final Map<String, Integer> symbols;
  private int nextAvailable;

  public SymbolTable() {
    symbols = new HashMap<>();
    nextAvailable = 16;
  }

  public void addSymbol(final String symbol, final int address) {
    if (symbol.matches("\\(\\w+\\)")) {
      final String label = symbol.substring(symbol.indexOf("(") + OFFSET,
          symbol.indexOf(")"));
      symbols.put(label, address);
    } else if (!symbol.matches("@[A-Z0-9_\\.$:]+")) {
      symbols.put(symbol, nextAvailable);
      nextAvailable++;
    }
  }

  public void addSymbol(final String symbol) {
    addSymbol(symbol, nextAvailable);
  }

  public boolean contains(final String symbol) {
    return symbols.containsKey(symbol);
  }

  public int getAddress(final String symbol) {
    return symbols.get(symbol);
  }
}
