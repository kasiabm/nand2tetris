package kasiabm.hackassembler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CodeTest {

  private Code code;

  @BeforeEach
  public void beforeEachTest() {
    code = new Code();
  }

  @Test
  public void shouldReturnBinaryRepresentationOfDest() {
    assertEquals("000", code.dest(null));
    assertEquals("001", code.dest("M=D"));
    assertEquals("010", code.dest("D=D+A"));
    assertEquals("011", code.dest("MD=A"));
    assertEquals("100", code.dest("A=A+1"));
    assertEquals("101", code.dest("AM=D"));
    assertEquals("110", code.dest("AD=M+1"));
    assertEquals("111", code.dest("AMD=1"));
  }

  @Test
  public void shouldReturnBinaryRepresentationOfJump() {
    assertEquals("000", code.jump(null));
    assertEquals("001", code.jump("D;JGT"));
    assertEquals("010", code.jump("M;JEQ"));
    assertEquals("011", code.jump("D;JGE"));
    assertEquals("100", code.jump("M;JLT"));
    assertEquals("101", code.jump("D;JNE"));
    assertEquals("110", code.jump("M;JLE"));
    assertEquals("111", code.jump("0;JMP"));
  }

  @Test
  public void shouldReturnBinaryRepresentationOfComp() {
    assertEquals("0101010", code.comp("D=0"));
    assertEquals("0111111", code.comp("D=1"));
    assertEquals("0111010", code.comp("D=-1"));
    assertEquals("0001100", code.comp("M=D"));
    assertEquals("0110000", code.comp("D=A"));
    assertEquals("1110000", code.comp("D=M"));
    assertEquals("0001101", code.comp("D=!D"));
    assertEquals("0110001", code.comp("D=!A"));
    assertEquals("1110001", code.comp("D=!M"));
    assertEquals("0001111", code.comp("M=-D"));
    assertEquals("0110011", code.comp("D=-A"));
    assertEquals("1110011", code.comp("D=-M"));
    assertEquals("0011111", code.comp("D=D+1"));
    assertEquals("0110111", code.comp("D=A+1"));
    assertEquals("1110111", code.comp("D=M+1"));
    assertEquals("0001110", code.comp("D=D-1"));
    assertEquals("0110010", code.comp("D=A-1"));
    assertEquals("1110010", code.comp("D=M-1"));
    assertEquals("0000010", code.comp("D=D+A"));
    assertEquals("1000010", code.comp("D=D+M"));
    assertEquals("0010011", code.comp("D=D-A"));
    assertEquals("1010011", code.comp("D=D-M"));
    assertEquals("0000111", code.comp("D=A-D"));
    assertEquals("1000111", code.comp("D=M-D"));
    assertEquals("0000000", code.comp("M=D&A"));
    assertEquals("1000000", code.comp("D=D&M"));
    assertEquals("0010101", code.comp("M=D|A"));
    assertEquals("1010101", code.comp("A=D|M"));
    assertEquals("0001100", code.comp("D;JGT"));
    assertEquals("0110000", code.comp("A;JEQ"));
    assertEquals("1110000", code.comp("M;JGE"));
  }

  @Test
  public void shouldThrowIllegalArgumentExceptionIfDestCommandUnknown() {
    assertThrows(IllegalArgumentException.class, () -> code.dest("B=A"));
  }

  @Test
  public void shouldThrowIllegalArgumentExceptionIfJumpCommandUnknown() {
    assertThrows(IllegalArgumentException.class, () -> code.jump("D;JNQ"));
  }

  @Test
  public void shouldThrowIllegalArgumentExceptionIfCompCommandUnknown() {
    assertThrows(IllegalArgumentException.class, () -> code.comp("A=B"));
  }
}
