package kasiabm.hackassembler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SymbolTableTest {

  private static final String VARIABLE = "@i";
  private static final String SYMBOL = "LOOP";
  private static final String LABEL = "(LOOP)";
  private static final int ADDRESS = 16;
  private SymbolTable symbolTable;

  @BeforeEach
  public void beforeTest() {
    symbolTable = new SymbolTable();
  }

  @Test
  public void shouldAddVariable() {
    symbolTable.addSymbol(VARIABLE);

    assertTrue(symbolTable.contains(VARIABLE));
    assertEquals(ADDRESS, symbolTable.getAddress(VARIABLE));
  }

  @Test
  public void shouldAddLabel() {
    symbolTable.addSymbol(LABEL);

    assertTrue(symbolTable.contains(SYMBOL));
  }

  @Test
  public void shouldReturnAddressOfSymbol() {
    symbolTable.addSymbol(LABEL, 0);

    assertEquals(0, symbolTable.getAddress(SYMBOL));
  }
}
