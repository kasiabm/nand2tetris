package kasiabm.hackassembler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BinaryCommandTest {

  private BinaryCommand binaryCommand;
  private SymbolTable symbolTable;
  private Code code;
  private int counter;

  @BeforeEach
  public void before() {
    binaryCommand = new BinaryCommand();
    symbolTable = new SymbolTable();
    code = new Code();
    counter = 0;
  }

  @Test
  public void shouldReturnBinaryRepresentationOfACommand() {
    final String command = "@5";

    assertEquals("0000000000000101",
        binaryCommand.toBinary(command, counter, symbolTable, code));
  }

  @Test
  public void shouldReturnBinaryRepresentationOfCCommandWithoutJump() {
    final String command = "D=D+A";

    assertEquals("1110000010010000",
        binaryCommand.toBinary(command, counter, symbolTable, code));
  }

  @Test
  public void shouldReturnBinaryRepresentationOfCCommandWithJump() {
    final String command = "D;JGT";

    assertEquals("1110001100000001",
        binaryCommand.toBinary(command, counter, symbolTable, code));
  }
}
