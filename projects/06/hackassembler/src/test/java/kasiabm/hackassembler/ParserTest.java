package kasiabm.hackassembler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParserTest {

  private static final Path RESOURCE_DIRECTORY =
      Paths.get("src", "test", "resources");
  private Parser parser;

  @BeforeEach
  public void before() {
    parser = new Parser();
  }

  @Test
  public void shouldGenerateHackFile() throws IOException {
    parser.parse(RESOURCE_DIRECTORY + "/Add.asm");

    final Path path = Paths.get(RESOURCE_DIRECTORY + "/Add.hack");
    assertTrue(Files.exists(path));
  }

  @Test
  public void shouldPopulateHackFileWithContent() throws IOException {
    final Path expected = Paths.get(RESOURCE_DIRECTORY + "/AddExpected.hack");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser.parse(RESOURCE_DIRECTORY + "/Add.asm");

    final Path actual = Paths.get(RESOURCE_DIRECTORY + "/Add.hack");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i),
            actualContent.get(i)));
  }

  @Test
  public void shouldGenerateHackFileWithLabels() throws IOException {
    final Path expected = Paths.get(RESOURCE_DIRECTORY + "/MaxExpected.hack");
    final List<String> expectedContent = Files.readAllLines(expected);

    parser.parse(RESOURCE_DIRECTORY + "/Max.asm");

    final Path actual = Paths.get(RESOURCE_DIRECTORY + "/Max.hack");
    final List<String> actualContent = Files.readAllLines(actual);
    IntStream.range(0, expectedContent.size())
        .forEach(i -> assertEquals(expectedContent.get(i),
            actualContent.get(i)));
  }
}
