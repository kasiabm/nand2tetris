// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

@8192
D=A
@wordCount
M=D // wordCount = 8192

(LOOP)
  @pointer
  M=0 // pointer = 0
  
(INNER_LOOP)
  @KBD
  D=M
  @BLACK
  D;JNE // if KBD != 0 GOTO BLACK
  @WHITE
  D;JEQ // if KBD == GOTO WHITE
  
(BLACK)
  @pointer
  D=M
  @SCREEN
  A=A+D // point to the next word
  M=-1
  @END // GOTO END
  0;JMP

(WHITE)
  @pointer
  D=M
  @SCREEN
  A=A+D // point to the next word
  M=0
  @END // GOTO END
  0;JMP

(END)
  @pointer
  MD=M+1 // pointer++
  @wordCount
  D=D-M
  @LOOP
  D;JEQ // if wordCount - pointer == 0 GOTO LOOP
  @INNER_LOOP
  0;JMP // else GOTO INNER_LOOP
